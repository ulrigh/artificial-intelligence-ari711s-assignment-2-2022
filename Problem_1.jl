### A Pluto.jl notebook ###
# v0.18.4

using Markdown
using InteractiveUtils

# ╔═╡ 0b5c1786-3bcc-45f8-8763-9d9ba104fc58
md"# Create Class Model Of Actors"

# ╔═╡ 41a38af9-9ba0-4ae2-9a97-371bb0e7a2c6
begin
	
	mutable struct Office
		itemCount::Int64 
		Position::Tuple{Int64, Int64}
	end
 
		struct Action
		Direction::String
		Move::Tuple{Int64, Int64}
		Cost::Int64
	end
	
	mutable struct AgentState
		Building::Matrix{Int64}
		StateCost::Int64
		collectedParcels::Int64
		CurrentOffice::Office
		path::Vector{String}
	end
	
end

# ╔═╡ 3c1fdbae-ceff-4f81-b629-db1e9d1b5c8c
md"# Assign intital state values"

# ╔═╡ 83b4af41-ba28-43b1-b73a-5105a09f86db
begin
	
	global Mu = Action("Up",(-1,0)		, 1)
	global Md = Action("Down",(1,0)		, 1)
	global Mw = Action("West",(0,-1)	, 2) #Left  <=
	global Me = Action("East",(0,1)		, 2) #Right =>
	global Co = Action("Collect",(0,0)  , 5)
	 
	global start = Office(0, (1,1)) 
	
	global ParcelsToBeCollected =  4 #Number Of Parcels To Be Collected 
	
	global CompanyX_Building = 
	   [0 0 0;
        1 0 0; 
        0 1 0;
        1 1 0;
        0 0 0]  
	
	global intialState = AgentState(CompanyX_Building, 0, 0, start,  []) 

	global moves = 0
	global goal =  false
	global CanCollect = true 
	global checkVal = false 
	global validState = true
 
	global PrevStatePosition = start.Position 
	global VisitedNodes = []
	push!(VisitedNodes, intialState)
	global CurrentState = () 
	global Path = []; 
 
end

# ╔═╡ c144a768-3f4c-40a4-9235-24339c7811fa
md"# Get Heuristic => f(n) = g(n) + h(n)"

# ╔═╡ 5a9ebce0-999e-4359-928e-777bc34cf9e9
md"# Declaring Functions"

# ╔═╡ 277a83eb-eb75-43a7-ab28-f673a5b6d04d
function getTotalCost(m, cost, n ,nextMoveCost) 

	gn = cost + nextMoveCost 
	
	if m[n[1],n[2]] >= 1 && CanCollect

		
		hn = (ParcelsToBeCollected - 1 ) * 3
	
	else
	
		hn = ParcelsToBeCollected * 3
		
	end 
	fn = gn + hn
	
	return fn
	
end

# ╔═╡ b29d884e-7696-4b2a-bb42-ca718f0ad637
 function stateIsValid(PrevState, CurrentState)
	 
	 if PrevState.CurrentOffice.Position != start
	 
		 if PrevState.CurrentOffice.Position == CurrentState.CurrentOffice.Position
	 
			 if PrevState.collectedParcels == CurrentState.collectedParcels 
			 
				 validState = false
			 
			 end
		 end
	 end
	 validState = true
 end

# ╔═╡ 868e9ff1-12b5-4d59-83a2-26211dc8b1de
function getAvailableMovesList(TempState)

Available_Moves = Action[]
	
	tempP_Mu = 
		TempState.CurrentOffice.Position[1] + Mu.Move[1] , 
		TempState.CurrentOffice.Position[2] + Mu.Move[2] 

	tempP_Md = 
		TempState.CurrentOffice.Position[1] + Md.Move[1] , 
		TempState.CurrentOffice.Position[2] + Md.Move[2] 
	
	tempP_Me = 
		TempState.CurrentOffice.Position[1] + Me.Move[1] , 
		TempState.CurrentOffice.Position[2] + Me.Move[2] 
	
	tempP_Mw = 
		TempState.CurrentOffice.Position[1] + Mw.Move[1] , 
		TempState.CurrentOffice.Position[2] + Mw.Move[2] 

	n = TempState.CurrentOffice.Position
	
	if TempState.Building[n[1],n[2]] >= 1 && CanCollect
	push!(Available_Moves, Co)
	end
	
	
	if 1 <= tempP_Mw[1] && 1 <= tempP_Mw[2]
		push!(Available_Moves, Mw)
	end
	
	if 1 <= tempP_Md[1] && 1 <= tempP_Md[2]
		push!(Available_Moves, Md)
	end
	
	if 1 <= tempP_Mu[1] && 1 <= tempP_Mu[2]
		push!(Available_Moves, Mu)
	end
 
	if 1 <= tempP_Me[1] && 1 <= tempP_Me[2]
		push!(Available_Moves, Me)
	end

	return Available_Moves
end

# ╔═╡ 9cac9b9e-f378-4f00-98d8-9b84fabfe63f
function buildNewState(CompanyX_Building,hurisiticVal,parcelsCollected,Coffice,Cpath, nextM, action)
	
	if action == Co
		x = CompanyX_Building[Coffice.Position[1], Coffice.Position[2]]
		
		CompanyX_Building[Coffice.Position[1], Coffice.Position[2]] = x - 1
 	
		canCollect = false
		
	end
		
	a = []

	for i in Cpath
		push!(a, i)
	end

		push!(a, nextM)
	
	newState = AgentState(CompanyX_Building,hurisiticVal,parcelsCollected,Coffice,a)
	
	return newState
	end

# ╔═╡ 0f71a02c-8729-4ff6-971a-d41003440a3d
function startSearch(state) 
	stateList = []
	parcels = 0 
	AvailableMoves = getAvailableMovesList(state) 
 
	for i in AvailableMoves 
		
		TempState = state;
	
		if i == Co
			
			
				println("Collecting...")
				
			 MX = TempState.Building
			
			 P_C = TempState.collectedParcels + 1
			
			 P =	
				(TempState.CurrentOffice.Position[1] + i.Move[1] , TempState.CurrentOffice.Position[2] + i.Move[2])
			
			 C_O = Office(TempState.Building[P[1],P[2]], P)
			
			 H_V = getTotalCost(TempState.Building,TempState.StateCost, P, i.Cost)
			 O_P = TempState.path
			 C_P =  i.Direction
							
			if stateIsValid(state, buildNewState(MX,H_V,P_C,C_O,O_P,C_P, i))
				push!(stateList, buildNewState(MX,H_V,P_C,C_O,O_P,C_P, i))
			end
			
			CanCollect = false;
		  
		else
			MX = TempState.Building
			 P_C = TempState.collectedParcels
			
			 P =	
				(TempState.CurrentOffice.Position[1] + i.Move[1] , TempState.CurrentOffice.Position[2] + i.Move[2])
			
			 C_O = Office(TempState.Building[P[1],P[2]], P)
			 H_V = getTotalCost(TempState.Building,TempState.StateCost, P, i.Cost)
			 O_P = TempState.path
			 C_P =  i.Direction
		
		CanCollect = true;
		
			if stateIsValid(state, buildNewState(MX,H_V,P_C,C_O,O_P,C_P, i))
				push!(stateList, buildNewState(MX,H_V,P_C,C_O,O_P,C_P, i))
			end
		end
	end

	sort!(stateList, by = x -> x.StateCost)  
	sort!(stateList, by = x -> x.collectedParcels, rev=true) 
	return stateList
end

# ╔═╡ 3057e251-74c2-412f-a1eb-98863fe30da5
function run(maze, state)
	TempState = state
	nodeList = []
    push!(nodeList, state)
	
	while nodeList[1].collectedParcels != ParcelsToBeCollected
		TempNodeList = startSearch(nodeList[1])  
		empty!(nodeList)

		for i in TempNodeList
		push!(nodeList, i)
		end
	end
	
	return nodeList
end

# ╔═╡ 780791bb-cc84-4170-844d-1b866233bf85
md"# Start A* Search"

# ╔═╡ 060d194c-ff5b-44e4-bd0c-aa7a854317c3
begin
	res = run(CompanyX_Building, intialState)
end

# ╔═╡ Cell order:
# ╠═0b5c1786-3bcc-45f8-8763-9d9ba104fc58
# ╠═41a38af9-9ba0-4ae2-9a97-371bb0e7a2c6
# ╠═3c1fdbae-ceff-4f81-b629-db1e9d1b5c8c
# ╠═83b4af41-ba28-43b1-b73a-5105a09f86db
# ╟─c144a768-3f4c-40a4-9235-24339c7811fa
# ╟─5a9ebce0-999e-4359-928e-777bc34cf9e9
# ╟─277a83eb-eb75-43a7-ab28-f673a5b6d04d
# ╟─b29d884e-7696-4b2a-bb42-ca718f0ad637
# ╟─868e9ff1-12b5-4d59-83a2-26211dc8b1de
# ╟─9cac9b9e-f378-4f00-98d8-9b84fabfe63f
# ╟─0f71a02c-8729-4ff6-971a-d41003440a3d
# ╟─3057e251-74c2-412f-a1eb-98863fe30da5
# ╠═780791bb-cc84-4170-844d-1b866233bf85
# ╠═060d194c-ff5b-44e4-bd0c-aa7a854317c3
