### A Pluto.jl notebook ###
# v0.19.4

using Markdown
using InteractiveUtils

# ╔═╡ b26790b1-0006-48cf-96c8-4258ddc5757e
# using CSP

# ╔═╡ ff96b8f5-799e-4ac3-b1b3-c2c9c66a8661
begin

	mutable struct D_Function
		Signature::Tuple{String, String}
		minimum_response_time::Float64
		maximum_latency::Int64
		minimum_throughput::Int64
		minimum_success_rate::Int64
	end
	
	mutable struct CSPVariables
		name::String
		Signature::Tuple{String, String}
		value::Union{Nothing, D_Function}
		F_Values::Vector{D_Function}
		DR_count::Int64  
	end
	
	struct FunctionCSP
		vars::Vector{CSPVariables}
		constraints::Vector{Tuple{CSPVariables,CSPVariables}}
	end


	

	#Create Action Constraint Graph
 
	
end

# ╔═╡ 096c4e6c-db02-4b0a-8f08-c78e0a89fef2
begin 
	begin
	F1 = D_Function(("String", "Int"), 3, 6, 9,12)
	F2 = D_Function(("String", "String"), 1, 2, 3, 5)
	F3 = D_Function(("Int", "Int"), 4, 43, 6, 4)
	F4 = D_Function(("String", "Int"), 12, 9, 6,3)
	
	FunctionDomain = [ F1, F2, F3, F4 ]
	
	end




function solve_csp(pb::FunctionCSP, all_assignments)
	for cur_var in pb.vars
		if cur_var.DR_count == 4
			 return []
		else
			next_val = rand(setdiff(Set([F1,F2,F3,F4]),Set(cur_var.F_Values)))
			cur_var.value = next_val
			
		#forward check
		for cur_constraint in pb.constraints
			
		# Check if neighbor works out
		if !((cur_constraint[1] == cur_var) || (cur_constraint[2] == cur_var))
			# println("--------------------------------------")
			# println(cur_var)
			# println("--------------------------------------")
			continue
		else
			
		if cur_constraint[1] == cur_var 
			push!(cur_constraint[2].F_Values, next_val)
			cur_constraint[2].DR_count += 1
		# if the count reaches four you should backtrack
			if cur_constraint[2].DR_count == 4
				solve_csp(pb,all_assignments)
			end
		# if the domain is a singleton (count == 3) propagate
			# if cur_constraint[2].DR_count == 3
			# 	return
			# end
			else
			push!(cur_constraint[1].F_Values, next_val)
			cur_constraint[1].DR_count += 1
		# if the count reaches four you should backtrack
			if cur_constraint[1].DR_count == 4
				solve_csp(pb,all_assignments)
			end
		# if the domain is a singleton (count == 3) propagate
			# 	if cur_constraint[2].DR_count == 3
			# 	return
			# end
				end
			end
		end
		  push!(all_assignments, cur_var.name => next_val)
			
	end
end
	return all_assignments
end



begin
	x1 = CSPVariables("X1",("String", "Int"), nothing, [], 0)
	x2 = CSPVariables("X2",("Int", "Int"), nothing, [], 0)
	x3 = CSPVariables("X3",("String", "String"), nothing, [], 0)
	x4 = CSPVariables("X4",("Int", "String"), nothing, [], 0)
	
	x5 = CSPVariables("X5",("Int", "String"), nothing, [], 0)
	x6 = CSPVariables("X6",("String", "String"), nothing, [], 0)
	x7 = CSPVariables("X7",("Int", "String"), nothing, [], 0)
end


	
end

# ╔═╡ ac464ef7-9502-4f06-9bf1-e10bfdf77eaf
	problem = FunctionCSP([x1, x2, x3, x4, x5, x6, x7], [(x1,x2), (x1,x3), (x1,x4),
(x1,x5), (x1,x6), (x2,x5), (x3,x4), (x4,x5), (x4,x6), (x5,x6), (x6,x7)])

# ╔═╡ a92a2838-a251-4cfe-9ba7-7bd21f7e5d52
begin 
	

	
	x =  solve_csp(problem, [])

	# for i in x 
	# 	print(i)
	# end

end

# ╔═╡ d3a4199d-56bd-4113-ac11-5dc1bd7e585a


# ╔═╡ Cell order:
# ╠═b26790b1-0006-48cf-96c8-4258ddc5757e
# ╠═ff96b8f5-799e-4ac3-b1b3-c2c9c66a8661
# ╠═096c4e6c-db02-4b0a-8f08-c78e0a89fef2
# ╠═ac464ef7-9502-4f06-9bf1-e10bfdf77eaf
# ╠═a92a2838-a251-4cfe-9ba7-7bd21f7e5d52
# ╠═d3a4199d-56bd-4113-ac11-5dc1bd7e585a
